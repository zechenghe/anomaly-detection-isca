\section{Architecture} \label{sec:implementation}

\subsection{Hardware Goals}

Having found a good algorithm and enhancement to give good accuracy for anomaly detection, we now move to step 3 of our methodology in Section \label{sec:methodology}. We have two goals in proposing a deep learning functional unit (DLU) for local anomalous behavior detection in the PLC. First the deployment of DLU should reduce the attack surface. Transferring data to the remote server introduces attack vectors within the wireless communication and the SCADA network and also incurs the round-trip latency. Second the DLU should be as small and low-cost as possible while meeting the computational requirements. This prevents the auxiliary hardware unit for detection from dominating the design cost of a PLC system. For instance, if a detection module requires as much logic and memory resources as the processing components in the original PLC, it will double the cost of the processing subsystem and is likely to be rejected by power grid service providers.

\subsection{System Architecture}

To enable the detection of anomalous behavior using HPCs, we present two ways, in Figure~\ref{img_system_diagram}, to integrate the deep learning functional unit (DLU) into the PLC system. 

A DLU integrated with method~\ding{172} reads the HPC values through the available interface/port. This function has already been supported by some contemporary PLC products like the WAGO PLC which we've used to collect the HPC readings for our experiments. This method gives the highest compatibility with existing PLCs. 

When designing new PLCs with the security feature of DLU, the method~\ding{173} to establish a DLU-CPU connection interface will be more desirable. The integration of a heterogeneous unit to a CPU will further reduce the attack surface during data transfer and at the same time reduce the detection latency. The implementation can leverage CPU-coprocessor interfaces similar to the RoCC interface for open-source RISC-V processors~\cite{asanovic2016rocket}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{./figures/plc_system_diagram.pdf}
    \caption{Integrating DLU to existing system architecture for detection.}
    \label{img_system_diagram}
\end{figure}

\iffalse
\subsection{System Configuration}\label{sec:implementation:config}

We implement a prototype of our proposed detection system. Our target system is a real power-grid controller sub-system, running the real deployed power-grid controlling code. The control codes are written in Structured Text (IEC 61131-3 standard), and run on the PLC as a multi-threaded program (e.g., 23 threads as I/O, controls, etc.). The 23 threads in the Wago PLC process being monitored correspond to the main thread and 22 threads. When the PLC powers on, 7 threads are created. When an application is loaded, another 15 threads will be killed and re-spawned with a different thread ID (TID). The last thread (PLC\_Task) is the one specifically running the loaded application (utilizing the other threads to do various tasks). We list the threads and their start phase in Table \ref{tab:threads}. 

\begin{table}[h]
\centering
\caption{Threads running on Programmable Logic Controller}\label{tab:threads}
\scriptsize
\begin{tabular}{| C{0.2\linewidth}| C{0.7\linewidth} |} 
\hline
  Start Time       & Thread(s) \\ \hline
  PLC powers on (7) & spi1, codesys3, com\_DBUS\_worker, 0ms\_Watch\_Thread, CAAEventTast, SchedExeption, Schedule \\ \hline
  Application loaded (15) & WagoAsyncRtHigh, WagoAsyncRtMed, WagoAsyncRtLow, WagoAsyncHigh, WagoAsyncMed, WagoAsyncLow, WagoAsyncBusCyc, WagoAsyncBusEvt, WagoAsyncBusEvt, WagoAsyncBusEvt, WagoAsyncBusEvt, ProcessorLoadWa, KBUS\_CYCLE\_TASK, ModbusSlaveTCP, PLC\_Task \\ \hline
  Main thread (1) & Main \\ \hline
\end{tabular}
\end{table}


The HPCs are monitored on a real PLC with an ARM Cortex A8 processor and a custom real-time Linux distribution. Data from 4 HPCs was collected for each thread: \ding{172} the number of cycles and \ding{173} the number of instructions executed, to provide the overall running status of the thread. \ding{174} The number of L1 cache misses, to reflect the locality and temporal properties of data usage. \ding{175} The number of branch instructions, to show the changes in the control flow of the threads.

%\begin{itemize}
%\item Number of cycles executed, $N_C$
%\item Number of instructions executed, $N_I$
%\item Number of branches executed, $N_B$
%\item Number of L1 cache misses, $N_L$
%\end{itemize}


The total number of HPCs monitored is 4N if there are N threads. However, as the ARM processor has only 2 read ports, only 2 of the 4 desired HPCs are read simultaneously and then switch to read the other 2. The HPCs are sampled at 1 kHz for each of the 23 threads.

We test the system with six different real attacks in Table \ref{tab:attacks}, that are known to cause damage to the power grid. The attacks include simulating the publicly reported malicious functionalities of Stuxnet \cite{langner2011stuxnet} and BlackEnergy \cite{2015powergridattack}, i.e., halting the controller and replaying prerecorded inputs to the legitimate code. Besides, we consider other attacks against the input, output and control logic of the processor, which are the critical components of the control code. We sample 300,000 samples for each attack running. 

\begin{table*}[h]
\centering
\caption{Baseline and attacks against a power-grid PLC}\label{tab:attacks}
\resizebox{\textwidth}{!}{
\begin{tabular}{| c | p{0.65\linewidth} | p{0.15\linewidth} |}
\hline
    & \textbf{Description} & \textbf{Hijack Type}\\ \hline
Baseline (Testing Normal)  & Baseline (a Proportional Integral Derivative (PID) controller) & None \\ \hline
Attack 1  & Overwrite the input (an additional line of code to overwrite the value of the input) & Input hijack \\ \hline
Attack 2  & Saturate the input (2 additional lines of code with IF condition on input value) & Control flow hijack \\ \hline
Attack 3  & Disable the PID control code (the entire PID block is commented out) & Entire Code hijack \\ \hline
Attack 4  & PID in "manual" mode (output is fixed) & Output hijack \\ \hline
Attack 5  & 2 PIDs Cascaded (input of a PID controller is sent through a second PID controller) & Control flow hijack \\ \hline
Attack 6  & Overwrite the output (an additional line of code to overwrite the value of the output) & Output hijack \\ \hline
\end{tabular}
}
\end{table*}
\fi


\subsection{Deep Learning Unit}

We design a minimalist yet flexible hardware module to enable the always-on anomaly detection. The module incurs minimal resource overhead by implementing only the essential computation primitives needed by LSTM inference and KS testing. The primitives of a detection model are encoded into a short program consisting of customized instructions. The short program is loaded with the model parameters into the FPGA to drive the hardware module for LSTM inference.

The module features a parallel architecture that does a full vector operation or matrix-vector multiply operation, over all the parallel tracks. Figure~\ref{img_blockdiagram} shows a module with four parallel tracks that loop over the matrix and vector four elements at a time. The level of parallelism, i.e. the number of parallel tracks in EXE stages, can be scaled to more parallel tracks if needed.

The format of a 128-bit instruction for controlling the computation is shown in Table~\ref{tbl_inst_fmt}. The~\textbf{Mode} field of an instruction defines the operation that will be performed by the datapath pipeline. Table~\ref{tbl_mode_lst} shows a list of operations in DLU. Section~\ref{sec_lstm_hwsupport} and Section~\ref{sec_ks_hwsupport} explain how they are used to support LSTM model inference and the KS test. A single instruction invokes a multi-cycle execution in this mode for initializing a finite state machine for countdown in the decode state. The number of iteration cycles is the same as the entire length of the vector or the size of matrix operand, which is encoded in the ~\textbf{Length} and ~\textbf{Width} fields.

%The module is driven by this program so that it is also programmable to support various models.

\iffalse
\begin{table}[ht!]
\centering
\caption{Format of instructions read by the hardware module}
\label{tbl_inst_fmt}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Inst} & \textbf{{[}127:124{]}} & \textbf{{[}123:110{]}} & \textbf{{[}109:96{]}} & \textbf{{[}95:64{]}} & \textbf{{[}63:32{]}} & \textbf{{[}31:0{]}} \\ \hline
\textbf{Field} & \textbf{Mode}              & \textbf{Length}            & \textbf{Width}            & \textbf{Addr\_X}         & \textbf{Addr\_Y}         & \textbf{Addr\_Z}        \\ \hline
\end{tabular}
\end{table}
\fi

\begin{table}[ht!]
\centering
\caption{Format of instructions read by the DLU}\label{tbl_inst_fmt}
\resizebox{0.48\textwidth}{!}{
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{Inst{[}127:124{]}} & \textbf{Inst{[}123:110{]}} & \textbf{Inst{[}109:96{]}} & \textbf{Inst{[}95:64{]}} & \textbf{Inst{[}63:32{]}} & \textbf{Inst{[}31:0{]}} \\ \hline
\textbf{Mode}              & \textbf{Length}            & \textbf{Width}            & \textbf{Addr\_X}         & \textbf{Addr\_Y}         & \textbf{Addr\_Z}        \\ \hline
\end{tabular}
}
\end{table}

% Please add the following required packages to your document preamble:
% \usepackage[normalem]{ulem}
% \useunder{\uline}{\ul}{}
\begin{table}[]
\caption{Operation modes supported by the DLU}\label{tbl_mode_lst}
\begin{tabular}{|l|l|}
\hline
\begin{tabular}[c]{@{}l@{}}Mode\\ Name\end{tabular} & Description                                                                                             \\ \hline
Vadd                                                & Element-wise addition of two vectors                                                                    \\ \hline
Vsub                                                & Element-wise subtraction of two vectors                                                                 \\ \hline
Vmul                                                & Element-wise multiplication of two vectos                                                               \\ \hline
Vsgt                                                & Element-wise set-greater-than of two vectors                                                            \\ \hline
Vact0                                               & Sigmoid function of a vector                                                                            \\ \hline
Vact1                                               & Tanh function of a vector                                                                               \\ \hline
MVmul                                               & Multiplication of a matrix and a vector                                                                 \\ \hline
Vmaxabs                                             & Find the maximum absolute value of a vector                                                             \\ \hline
VSsgt                                               & \begin{tabular}[c]{@{}l@{}}Set-greater-than to compare a vectors' \\ elements and a scalar \end{tabular} \\ \hline
\end{tabular}
\end{table}

%Table~\ref{tbl_inst_fmt} shows the 128-bit instruction format. The~\textbf{Mode} field defines the memory-to-memory operation that will be performed by the detection module. The~\textbf{Length} and~\textbf{Width} fields control the number of times for which the operation will be repeated. For a element-wise vector operation, the \textbf{length} field is set to be the length of input vector(s). In a matrix-vector operation,~\textbf{Length} and~\textbf{Width} represent the matrix size. An instruction taking multiple cycles to finish will stay in the pipeline until the counter of executed loops in the~\textbf{Decode} stage reaches the number specified by the~\textbf{Length} and~\textbf{Width} fields. Moreover, the module memory-to-memory operation

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{./figures/plc_block_diagram.pdf}
    \caption{Block diagram of a DLU}
    \label{img_blockdiagram}
\end{figure}

%The architecture of our anomaly detection module is plotted in Figure~\ref{img_blockdiagram}. The processing part of the module is pipelined with four execution stages. Look-up tables (LUTs) of non-linear functions are placed in the first execution stage. The second and third stages include multipliers (MULs) and adders (ADDs) respectively. The final execution stage is only used to latch the outputs to the external block RAM. Our design supports parallel processing and a current prototype has 4 parallel computing tracks, each having one LUT, one multiplier and one adder. 

Unlike other vector units, we use a memory-to-memory architecture rather than vector registers. We believe this gives a more flexible and simpler architecture. We use a software-managed memory, where we can lay out the memory contents based on matrix and vector sizes. We refer to operands by their addresses in this memory in the instruction.  In our FPGA implementation, the BRAM (block RAM) is used as the software-managed memory with 32 bits of address space.
%The architecture also performs vector operations in a memory-to-memory manner where we use a software-managed memory instead of vector registers. The operands are specified by their addresses in the instructions, which gives a more flexible architecture for efficiently implementing different algorithms without the complexity to manage the register. In our FPGA prototype implementation, the block RAM is used to store model parameters and intermediate variables during computation. 
%The memory space of the block RAM and the address generation are managed by the instructions of an ML/DL model for anomaly detection. 
There is also a buffer to store the HPC readings. The buffer can be accessed by the hardware module to load the incoming reading for anomaly detection.

%the software-defined memory stores the matrices, vectors and other parameters of the LSTM model, and is implemented in the FPGA’s local BRAM memory.  A chunk of this local memory is also reserved for double buffering of the inputs (in this case the HPC values for a window for prediction), so that one buffer is being used for the LSTM inferencing and KS testing while the other buffer is being filled from the new HPC values.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{./figures/lstm_combined.pdf}
    \caption{An LSTM cell (top) and mapping LSTM inference to our FPGA instructions (bottom). %For $*=f, i, o, n$, the weight matrices, $W_*$ and $U_*$, and the biases, $b_*$, %used to compute $PreAct_* (*=f, i, o, n)$ are concatenated as 
    }
    \label{img_lstm_eq_fig}
\end{figure}

\subsubsection{Support for LSTM Inference} ~\\
\label{sec_lstm_hwsupport}
Figure~\ref{img_lstm_eq_fig} shows an LSTM cell (top), and the operations used in LSTM inference. Model weights denoted by $W_* (* = f,i,o,n,p)$ and $U_* (* = f,i,o,n)$ are matrices and the biases denoted by $b_* (* = i,f,o,n,p)$ are vectors. Their sizes are hyper-parameters of the DL model. %Although an LSTM cell needs 8 matrix-vector multiplications and 8 vector-vector additions to 
When computing $PreAct_* (* = f,i,o,n)$ as the pre-activations in an LSTM cell, we optimize by concatenating the weight matrices, $W_*$ and $U_*$, and biases, $b_*$, into one large matrix to multiply with the concatenation of $x_t$, $h_{t-1}$ and $1$. This combination reduces the number of instructions from 8 matrix-vector multiplications and 8 vector additions to 1 matrix-vector multiplication. There are also vector additions, multiplications and 5 non-linear activation functions. % to compute a prediction, $P_{t+1}$, as the output at time t. 
These operations are common to other deep learning and machine learning algorithms. Hence we implement the following operation modes in the execution stages of the parallel datapath.

\bheading{Vector-vector operations.} The module can perform element-wise vector-vector operations like vector multiplication (\textbf{Vmul}) in EXE1 stage, or vector addition (\textbf{Vadd}) or subtraction (\textbf{Vsub}) in EXE2 stage.

%input vectors will be forwarded to the EXE2 stage for addition (\textbf{Vadd}). Subtraction (\textbf{Vsub}) is computed by feeding the negative of the second operand to the adder.

%\bheading{Element-wise multiplication of two vectors.} In vector multiplication mode (\textbf{Vmul}), the input vectors will be forwarded to the multiplier stage EXE1.

\bheading{Non-linear activation functions of a vector.} The LSTM model needs a vector activation mode (\textbf{Vact}) to compute non-linear functions, e.g. sigmoid and tanh. Rather than adding complicated functional units, we use linear interpolation to approximate the activation functions. When an input vector comes in, the slope, slp[i], and the intercept, b[i], of a piecewise linear approximation are read from a selected entry in a look-up table (LUT) in EXE0 stage according to the input value, X[i]. The result, Z[i], is computed in the following two stages (EXE1 and EXE2) as $Z[i] = slp[i] \times X[i] + b[i]$. %Different from \cite{Chen:2014:DSH:2541940.2541967} where the non-linear units are placed in the last stage, requiring extra multipliers and adders, our design reduces the function units by placing the LUTs in an earlier stage so that existing multipliers and adders in later stages are reused.

\bheading{Matrix-vector multiplication.} %In the LSTM model shown in Figure~\ref{img_lstm_eq_fig}, the~\textbf{MVmul} mode is needed to multiply feature vectors, e.g. $x_t$ and $h_t$, with weight matrices, e.g. $W_f$ and $U_f$. 
The datapath computes the multiply-accumulate operation with the multipliers and adders in EXE1 and EXE2 stages. We adopt loop tiling for matrix-vector multiplication \cite{Chen:2014:DSH:2541940.2541967} \cite{song2018situ} to reduce memory traffic by unrolling a loop and increasing data reuse. We optimize by adding a local scratchpad memory in EXE2 stage to save repetitive store-and-reload of partial sums in the multiplication from block RAM and reduce the read latency by temporarily storing the partial sums in the local scratchpad memory. We also optimize by making the adders in EXE2 stage controllable to act as an adder tree so that no extra adders are needed. During the computation, the partial sum is read from the local scratchpad and used as one of the inputs to the adder tree.

%\textbf{\ding{172} Adder reuse.} Without adding more adders to sum up the products, we make the adders in EXE2 stage controllable to act as an adder tree which takes the products and the previous partial sum as its input.

\iffalse
\begin{figure}[h]
\begin{lstlisting}[language=C, frame=single, xleftmargin=.5em, framexleftmargin=.5em, caption={Tiling for Matrix-vector Multiplication}, label={alg_tiling}]
  // N_local: size of local scratchpad
  // N_track: number of parallel computing tracks
  for (i = 0; i < L; i+= N_local)
    for (j = 0; j < W; j += N_track)
      for (ii = 0; ii < N_local; ii ++)
        // Computed every cycle
        for (jj = 0; jj < N_track; jj++)
          O[i*N_local+ii] += W[i*N_local+ii][j*N_track+jj] * I[j*N_track+jj]
\end{lstlisting}
\end{figure}
\fi

\iffalse
\begin{figure}[h]
\begin{lstlisting}[language=C, frame=single, xleftmargin=.5em, framexleftmargin=.5em, caption={KS-test Implementation}, label={alg_kstest}]
  // Step 1: Get testing cumulative histogram
  for (i = 0; i < L_test; i++)
    for (j = 0; j < L_ref; j ++)
      // Need vector-scalar comparison
      if test_err[i] < ref_bin[j]
        test_hist[j]++
  
  // Step 2: Compare with reference histogram
  for (i = 0; i < L_ref; i++)
    diff_hist[i] = ref_hist[i] - test_hist[i]
  
  // Step 3: Find maximum difference
  // Need to find the maximum in a vector
  diff_max = max(diff_hist)
  
  // Step 4: Report result
  if (diff_max > diff_th)
    report_anomaly(diff_max)
  else
    report_normal(diff_max)
\end{lstlisting}
\end{figure}
\fi

\subsubsection{Support for Statistic Test} ~\\
\label{sec_ks_hwsupport}
KS test determines whether the reference RED and the observed RED are the same distribution. The reference RED is collected in the training phase and represented by reference bin boundaries and a reference cumulative histogram. The observed RED is collected online and represented by a series of observed errors. 

We implement a five-step KS test module on FPGA and show an example of the workflow in Figure \ref{fig:ks_test}. The grey dotted boxes represent inputs and output. Step \ding{172}: compare an observed error with bin boundaries. The output of this step is a vector of ``0''s and ``1''s. ``1'' represents that the corresponding bin boundary is greater than the observed error, and ``0'' otherwise. This step requires a new operation for vector-scalar comparison (\textbf{VSsgt}). Step \ding{173}: accumulate all binary vectors from \ding{172}. The accumulated vector, namely ``observed histogram'', conceptually represents the cumulative histogram of the observed errors using the reference bins. Step \ding{174} and step \ding{175}: find the largest difference in the reference and observed histograms, i.e. KS statistics $D_{n,m}$ in Eq (\ref{equ:KSstatistic}). Step \ding{174} is a vector subtraction and step \ding{175} is a new operation, \textbf{Vmaxabs}, to find the maximum absolute value in a vector. Step \ding{176}: compare $D_{n,m}$ with a threshold, i.e. RHS of Eq (\ref{equ:KStestinequ}), to determine normality. A \textbf{VSsgt} instruction with a vector size of 1 is used for the scalar-scalar comparison in step \ding{176}. Hence, we introduce two new operation modes, \textbf{VSsgt} and \textbf{Vmaxabs}, to support KS testing:

%we perform a KS test for a series of computed errors by~\circled{1} getting the cumulative histogram of the current test error, ~\circled{2} adding the histogram of every input error to the test histogram,~\circled{3} computing the difference between the testing and the reference histograms,~\circled{4} finding the maximum absolute difference and~\circled{5} comparing it with a predefined threshold. To support this five-step statistical test, we add two more operation primitives,~\textbf{VSslt} and~\textbf{Vmaxabs}, which are mentioned in Figure~\ref{img_ks_test}:

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\linewidth]{./figures/ks_test.pdf}
    \caption{An example of five-step KS test. Two new operation primitives, \textbf{VSsgt} and \textbf{Vmaxabs}, are implemented in step \ding{173} and \ding{175} to support KS test, respectively.}
    \label{fig:ks_test}
\end{figure}

%circled{2}: needs~\textbf{Vadd} to accumulate the histogram of current error in the test histogram and it is already implemented for LSTM. The comparison with reference histogram in~\circled{3} requires~\textbf{Vsub} which has also been supported.~\circled{4} uses~\textbf{Vmaxabs} mode to find the maximum of absolute histogram difference, namely the $D_{n,m}$ in Eq~(\ref{equ:KSstatistic}).~\circled{5} computes Eq~(\ref{equ:KStestinequ}) with another~\textbf{VSslt} instruction with a length of one to compare $D_{n,m}$ with the threshold.}

\bheading{Vector-scalar comparison.} The vector-scalar comparison mode,~\textbf{VSsgt}, sets an output element to one if the corresponding element in the input vector is greater than the input scalar. During execution, the scalar is kept in the pipeline, and the bin boundaries are streamed in for comparison. Each adder does subtraction, and the output is decided by the sign of the difference. 
%This operation mode can also be used for the scalar-scalar comparison with threshold in Step 3.
%The output could thus be treated as the histogram for one error. 

\bheading{Find the maximum absolute value in a vector.} In~\textbf{Vmaxabs} mode, the datapath compares the absolute values of input elements in EXE2 stage by doing subtraction with the adders. A temporary maximum is stored in the local scratchpad as the loop on the vector can take multiple cycles to finish. Every cycle the temporary maximum is compared with the maximum of absolute values and gets updated.