\section{Hardware Evaluation}

\subsection{Performance Evaluation}
%\subsection{Performance Evaluation}

Figure \ref{fig:timing} details the time taken by our LSTM+KS architecture to detect an anomaly.

\bheading{\ding{172} LSTM Prediction Time $t_{predict}$.} This is the inference time for the LSTM model to predict the controller behavior, in terms of the HPC values for one reading.

\bheading{\ding{173} Sampling Interval $t_{sample}$.} This is the interval between two HPC readings.
%The accumulated number of corresponding hardware events are stored in the HPCs during the interval.

\bheading{\ding{174} Error Accumulation (EA) Time $t_{EA}$.} This is the time to gather one point of the reconstruction error. Note that in Eq (\ref{equ:reconstructionerror}), one point of the reconstruction error is accumulated among $L$ time frames. Therefore, $t_{EA}=max\{t_{predict}, t_{sample}\}\times L$.

\bheading{\ding{175} RED Collection Time $t_{RED}$.} This is the time to collect the testing reconstruction error distribution. This distribution is represented by $N_{EA}$ independent reconstruction errors in \ding{174}.

\bheading{\ding{176} KS-Test Time $t_{KS}$.} This is the time to compute the KS statistic and determine if an anomaly has occurred.

\begin{figure}[h]
 \centering
  \includegraphics[width=\linewidth]{figures/timing.pdf}
  \caption{Latency evaluation for our proposed architecture.}
  \label{fig:timing}
\end{figure}

%Furthermore, updating the model is faster than training from scratch. Therefore, we evaluate the time for training the model from scratch, and the latency for online detection.

%In each inference time period, we can infer multiple PLC sequences in parallel. 

%We can parallelize 256 PLC sequences, the degree of parallelization depends on the GPU we are running on. For each time frame, we need to predict 2000 time stamps in the future and subsample it, to ensure the independence required by KS test samples. Thus, the time taken to detect an attack is 2 seconds (2000 / 1 kHz), i.e. if an attack happens at $t$ seconds, it can be discovered at $t+2$ seconds as anomalous PLC behavior. In a real implementation, a trusted deep learning accelerator can be used (Figure \ref{fig:system}), which will give faster execution time and greater security through hardware isolation.

The detection latency $t_{detection}$, i.e. the time interval between the occurrence of the attack and the detection of the anomaly, is determined by $L, N_{EA}$ in the algorithm, and $t_{predict}$, $t_{sample}$ and $t_{KS}$ in the real implementation. Specifically,
{\small
\begin{align}
t_{detection}&= t_{RED}+t_{KS} \label{eq:tRED} \\ 
&= max\{t_{predict}, t_{sample}\}\times L\times N_{EA} + t_{KS}
\end{align}
}%

We observe that $t_{RED}$, proportional to $L \times N_{EA}$, dominates $t_{KS}$ in Eq (\ref{eq:tRED}). Thus we look for the minimal $L \times N_{EA}$ that maintains false positives rate (FPR) $<10^{-4}$. We start with L = 50 and $N_{EA}$ = 40 to give a detection time of about 2 seconds. Then, through trial-and-error, we reduce L or $N_{EA}$, while holding FPR $<10^{-4}$. We observe a significant FPR increase ($\sim$10.4\%) after $L \times N_{EA}$ = 250. Hence, we choose $L=10$ and $N_{EA}=25$ in our implementation.

%\begin{figure}[h]
% \centering
%  \includegraphics[width=0.8\linewidth]{figures/NL.pdf}
%  \caption{The highest accuracy for each $N_{EA}*L$. We observe a significant drop when $N_{EA}*L<200$. At this point, $L=25$ and $N_{EA}=10$}
%  \label{fig:NL}
%\end{figure}

The FPGA prototype is running at 125MHz and it has enough performance for our detection mechanism. Other implementations can increase the number of parallel tracks according to the target workload. We show our FPGA time measurements in Table \ref{tab:performance} row 1. We also implement the anomaly detection with data transferred to an application server (Figure \ref{fig:SCADA}) with a GPU, for comparison with our in-situ FPGA implementation, in row 2 of Table \ref{tab:performance}. For this, we implement the LSTM model on an Nvidia 1080Ti GPU. The server is equipped with 2 Intel Xeon E5-2667 2.90GHz CPUs, each with 6 cores, running on Ubuntu 14.04.5 LTS. The level 1 instruction and data caches are 32 KB, the level 2 cache is 256 KB and the level 3 cache is 16MB. 

We observe that $t_{predict} < t_{sample}$ using both the FPGA and GPU implementation, i.e. the bottleneck is the HPC sampling. However, note that using the server GPU introduces extra network transaction time $t_{network}$=100 ms round-trip. Training is typically done off-line only once on the application server, and needs to be updated only if the functionality of code is modified, while only inferencing is needed in real-time.

\begin{table}[h]
\centering
\caption{Execution time for model training and inferencing, and KS testing, for detecting controller anomalous behavior. In both implementation, $L=25$ and $N_{EA}=10$.}\label{tab:performance}
\resizebox{\linewidth}{!}{
\begin{tabular}{| c | c | c | c | c | c | c | c | c |} 
\hline
     & \textbf{Training}  & $t_{network}$ & $t_{sample}$ & $t_{predict}$ & $t_{EA}$     & $t_{RED}$ & $t_{KS}$         & \textbf{Detection} \\ \hline
\textbf{FPGA} & --   & --         & 1 ms         & 0.17 ms       & 25 ms        & 250 ms    &   10$\mu s$      & \textbf{250 ms}          \\ \hline
\textbf{GPU}  & \textbf{21.1 min}   & 100 ms   & 1 ms         & 0.53 ms       & 25 ms        & 250 ms    &   8.6 ms ${}^\star$ & \textbf{358.6 ms}        \\ \hline
\end{tabular}
}
\begin{tablenotes}
    \scriptsize
    \item ${}^{\star}$ KS test is implemented on the server CPU.
\end{tablenotes}
\end{table}

\subsection{Storage and FPGA requirements}

We implement a FPGA prototype of the hardware module. For deployment in the real world, it could be an ASIC, an FPGA or even an FPGA embedded into the processor (as available now in Intel chips). Our FPGA implementation is just for prototyping the performance and complexity of the hardware.

The platform board is Xilinx Zynq-7000 SoC ZC706 evaluation kit. The hardware implementation is generated with Vivado 2016.2. Due to the range of accumulated errors in the experiment, we choose 32-bit fixed-point numbers as a conservative choice to avoid number overflow. The power is profiled for the programmable logic (PL) of the board with TI Fusion Digital Power Designer.

We compare our prototype with the latest hardware accelerator for recurrent neural network (RNN) and LSTM. The utilizations of FPGA lookup tables (LUT), flip-flops (FF) and digital signal processing (DSP) units are much less than DeltaRNN~\cite{gao2018deltarnn} and C-LSTM~\cite{wang2018c} which are specialized for two kinds of RNN models. We expect a larger reduction and a higher clock frequency when the 16-bit fixed point number is used. From the functional perspective, our DLU design features the ability to directly return the detection result, e.g. by doing KS test, and the flexibility to support other detection models based on vector and matrix operations.
The results shows a major difference between performance-oriented design and the DLU for detection designed to be versatile and deliver enough performance.

\begin{table}[]
\centering
\caption{Hardware statistics compared to RNN accelerators}
\label{tbl_hw_stats}
\resizebox{0.48\textwidth}{!}{
\begin{tabular}{|l|l|l|l|}
\hline
                & DeltaRNN                                                                         & C-LSTM\footnotemark                                                         & Basic                                                                                 \\ \hline
Platform                                              & \begin{tabular}[c]{@{}l@{}}Xilinx Zynq-7000 All \\ Programmable SoC\end{tabular}               & \begin{tabular}[c]{@{}l@{}}Alpha Data's\\ ADM-7V3\end{tabular}                  & \begin{tabular}[c]{@{}l@{}}Xilinx Zynq-7000 \\ SoC ZC706\\ Evaluation Kit\end{tabular}                                                                  \\ \hline
FPGA                                                  & Kintex-7 XC7Z100                                                                               & Virtex-7 (690t)                                                                 & XC7Z045 FFG900                                                                                                                                          \\ \hline
Design Tool                                           & Vivado 2017.2                                                                                  & Xilinx SDx 2017.1                                                               & Vivado 2016.2                                                                                                                                           \\ \hline
Quantization                                          & 16-bit fixed point                                                                             & 16-bit fixed point                                                              & 32-bit fixed point                                                                                                                                      \\ \hline
Slice LUT                                             & \begin{tabular}[c]{@{}l@{}}261,357\\ (32.51X)\end{tabular}                                     & \begin{tabular}[c]{@{}l@{}}406,861\\ (50.61X)\end{tabular}                      & 8,039                                                                                                                                                   \\ \hline
Slice FF                                              & \begin{tabular}[c]{@{}l@{}}119,260\\ (31.54X)\end{tabular}                                     & \begin{tabular}[c]{@{}l@{}}402,876\\ (106.55X)\end{tabular}                     & 3,781                                                                                                                                                   \\ \hline
DSP                                                   & \begin{tabular}[c]{@{}l@{}}768\\ (48.00X)\end{tabular}                                         & \begin{tabular}[c]{@{}l@{}}2675\\ (167.18X)\end{tabular}                        & 16                                                                                                                                                      \\ \hline
BRAM                                                  & \begin{tabular}[c]{@{}l@{}}457.5\\ (0.94X)\end{tabular}                                        & \begin{tabular}[c]{@{}l@{}}966\\ (1.98X)\end{tabular}                           & 489                                                                                                                                                     \\ \hline
\begin{tabular}[c]{@{}l@{}}Clock\\ (MHz)\end{tabular} & 125                                                                                            & 200                                                                             & 115                                                                                                                                                     \\ \hline
Power (W)                                             & \begin{tabular}[c]{@{}l@{}}FPGA Idle: 1.8\\ FPGA Run: 7.3\end{tabular}                         & Platform: 22                                                                    & \begin{tabular}[c]{@{}l@{}}FPGA Idle: 0.12\\ FPGA Run: 0.62\end{tabular}                                                                                \\ \hline
Versatility                                           & \begin{tabular}[c]{@{}l@{}}Specialized for \\ Gated Recurrent Unit\\ RNN dataflow\end{tabular} & \begin{tabular}[c]{@{}l@{}}Specialized for \\ LSTM RNN \\ dataflow\end{tabular} & \begin{tabular}[c]{@{}l@{}}Flexible for RNN \\ and other models\\ based on vector and\\ matrix operations.\\ Extra support for \\ KS test.\end{tabular} \\ \hline
\end{tabular}
}
\end{table}


\footnotetext{The resource utilization number is computed from the utilization ratio in the original paper}

%\subsection{Storage and FPGA requirements}
%The trained deep learning model and the reference RED need to be stored. In our FPGA implementation, the deep learning model is an LSTM with 128 hidden nodes, 325.1KB in size. The reference RED is less than 1KB in size. In total, we need $<$ 326 KB storage, which fits in our FPGA BRAM memory. Our implemented detection module, equipped with four computing tracks, uses 6.5k FPGA LUTs, 4.2k FPGA flip-flops, 16 DSPs and 281 block RAM units. Compared to other published FPGA designs for LSTM computation \cite{gao2018deltarnn}, our LSTM detection module uses much fewer FPGA resources, while still providing enough performance for anomaly detection in power-grid systems.

%performance-oriented designs for LSTM computation \cite{gao2018deltarnn}, the detection module provides enough performance for anomaly detection in power-grid systems, but uses 40.2X, 28.4X, 48X and 1.63X fewer corresponding resources. 

%It shows a trade-off of hardware support for DL applications when the performance is not the only dominating factor to consider.

%Our implemented detection module, equipped with four computing tracks, uses 6.5k FPGA LUTs, 4.2k FPGA flip-flops, 16 DSPs and 281 block RAM modules, i.e. 40.2X, 28.4X, 48X and 1.63X fewer than performance oriented design for LSTM computation \cite{gao2018deltarnn}. It demonstrates a trade-off of hardware support for DL applications when the performance is not the only dominating factor to consider.

%\begin{table}[h]
%\centering
%\caption{Execution time for model training and inferencing, and KS testing, for detecting the controller hijacking attacks.}\label{tab:performance}
%\resizebox{0.6\linewidth}{!}{
%\begin{tabular}{| c | c | c | c |} 
%\hline
%       & Training & Inference & KS Test \\ \hline
%LSTM & 21.1 min & 39.8 ms & 8.63 ms \\ \hline
%CRBM & 3.5 min & 265 ms & 8.63 ms \\ \hline
%\end{tabular}
%}
%\end{table}
