PAPER = main
TEX = $(wildcard *.tex)
BIB = anomaly.bib
FIGS = $(wildcard figures/*.pdf figures/*.png graphs/*.pdf graphs/*.png)

.PHONY: all clean

$(PAPER).pdf: $(TEX) $(BIB) $(FIGS)
	echo $(FIGS)
	pdflatex $(PAPER)
	bibtex $(PAPER)
	pdflatex $(PAPER)
	pdflatex $(PAPER)
	open $(PAPER).pdf
clean:
	rm -f *.aux *.bbl *.blg *.log *.out *.fls *.fdb_latexmk $(PAPER).pdf

