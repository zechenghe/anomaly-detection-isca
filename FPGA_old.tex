\section{Hardware Module for Anomaly Detection}

We design a hardware module to enable the always-on anomaly detection. The module incurs minimal resource overhead by implementing only the essential computation primitives needed by LSTM inferencing and KS-test. The module is designed to be microcode-driven so that it is also programmable to support various models. We prove in the evaluation section that the module has enough performance for our detection mechanism.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{./detection_module.pdf}
    \caption{Block diagram of the hardware module}
    \label{img_blockdiagram}
\end{figure}

The architecture of our anomaly detection module is plotted in Figure~\ref{img_blockdiagram}. The processing part of the module is pipelined to have four execution stages. Look-up tables (LUTs) of non-linear functions are placed in the first execution stage. The second and third stage include multipliers (MULs) and adders (ADDs) respectively. The final execution stage is only used to latch the outputs to the external block RAM. The block RAM is a piece of addressable memory used to stored model parameters and intermediate variables during computation. The memory space of the block RAM and the address generation are managed by the microcodes of a ML/DL model. There is also a buffer to store the incoming HPC readings. The buffer is connected to the hardware module and mapped to a different region in the microcode's address space. This enables the hardware module to be programmed to fetch the input features.

The microcode is designed to instruct the datapath to perform all the necessary operation primitives. The format of a microcode is According to the introduction of LSTM model, we incorporate the following operations: 

\begin{itemize}
\item \textbf{Element-wise addition and subtraction of two vectors.} The LSTM model requires addition operation to add bias vector to a feature vector and subtraction to compute the reconstruction error. The input vectors will be forwarded to the adder stage for addition. The subtraction is computed by feeding the opposite of the second operand to the adder.

\item \textbf{Element-wise multiplication of two vectors.} Element-wise multiplication is needed by the LSTM model to combine the gates and hidden states. The input vectors will be forwarded to the multiplier stage and the product will be write back as the result.

\item \textbf{Element-wise non linear function of a vector.} Activation functions like sigmoid and tanh are used in the LSTM model. Instead of adding complicated function units to compute a fixed non-linear function, we approximate the activation functions with linear interpolation. When an input vector comes in, the slope and the intercept of a piecewise linear approximation are selected from every LUT in the first execution stage according to the input value. The slope is multiplied with the input in EXE1 stage and the intercept is added to the product in EXE2 stage.

\item \textbf{Matrix-vector multiplication.} In the LSTM model, feature vectors need to be multiplied with weight matrices. To perform this operation, the detection module first computes the element-wise products of a row in the matrix and the vector in EXE1 stage. The adders in EXE2 stage are controlled to act as an adder tree which sums up the products. As this dot product computation always takes multiple cycles to finish, the module saves repetitive read-and-refetch of partial sums by storing them in the local scratchpad. During the computation, the partial sum will be read from the local scratchpad and used as one of the inputs to the adder tree.

\end{itemize}

We also consider the operations need by the KS-test where a reference reconstruction error distribution (RED) and a test RED are compared. The reference distribution is passed to the hardware module as part of the model. The distribution parameters include a list of discrete bin boundaries and a reference cumulative histogram. We perform a KS-test for a series of computed errors by (1) adding to the a histogram defined by the discrete bin boundaries for every input error (2) computing the difference between the test histogram and the reference histogram for all the bins and (3) finding the maximum difference. To support this three-step statistical test, we need two more operation primitives which could also by computed with our hardware module:

\begin{itemize}
\item \textbf{Vector-scalar comparison.} The test error needs to be compared with every bin boundary of the reference histogram and the result will be accumulated in the test histogram. The vector scalar comparison is performed as an element-wise set-larger-than. The scalar is kept in the pipeline and the bin boundaries are streamed in for comparison. Each adder computes a difference by doing subtraction and the output is set to one if the difference is positive or zero if negative. The output could thus be understood as the histogram for one error.

\item \textbf{Find the maximum of a vector.} To find the maximum of a vector, the detection module compares the input elements in EXE2 stage by doing subtraction with the adders. A temporary maximum needs to be stored in the local scratchpad as the traversal on the vector may take multiple cycles. In each cycle, the temporary maximum is also compared with the elements being processed and gets updated until there is no other input element.

\end{itemize}