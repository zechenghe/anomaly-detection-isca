\section{Introduction}\label{sec:intro}

Many papers have been proposed on accelerating different aspects of machine learning and deep learning algorithms. Much research has also been done on the security of different types of computer systems. However, how much deep learning is actually needed to achieve important security functions has not been studied. What important real-world security protections can be achieved by applying deep learning algorithms, and how much acceleration or hardware is really needed to achieve this? We propose a new methodology for applying deep learning to security and determining the complexity of the hardware accelerator module needed. For concreteness, we show a case study of one of the most important cyber-physical systems: a power-grid substation in the power-grid system. The security function we study is the detection of potential attacks on the power-grid.

The power-grid is a critical infrastructure, and attacks on it can cripple society, affecting national security, economic competitiveness and societal interactions. In 2015, Ukraine experienced cyber attacks against its power-grid system \cite{2015powergridattack}. During this attack, 30 substations were switched off and 230 thousand people were left without electricity. The U.S. Government Accountability Office (GAO) questioned the current adequacy of defenses against power-grid attacks, and the North American Electric Reliability Corporation (NERC) has recognized these concerns \cite{sridhar2012cyber}. With power-grid substations controllable through cyberspace transactions, the concern about cyber attacks on the physical power-grid systems is a real and serious threat, which is the focus of our paper. 
%Although these attacks can have impact, they are still an unsolved problem.

Figure \ref{fig:SCADA} shows a Supervisory Control And Data Acquisition (SCADA) system controlling a power-grid \cite{SCADA}. While the rest of the SCADA network and components are similar to general information processing systems, the power-plant substations are the cyber-physical systems we focus on. Power-grid systems can be attacked through cyber attacks or through physical attacks. In this paper, we focus on cyber attacks which affect the controller of a power-grid sub-system, called a Programmable Logic Controller (PLC) or Remote Control Unit (RTU). For simplicity, we use the name PLC in the rest of this paper. A PLC is a small computer system. The PLC obtains inputs from the sensors, and outputs control signals to the actuators according to its control code. Figure \ref{fig:PLCdiag} shows the block diagram of a PLC \cite{PLCdiag}. A PLC is composed of several subsystems, e.g. CPU, internal memories and communication interface. A PLC can take physical sensor (and other) inputs from the target industrial devices, make decisions and send commands to control a large range of devices and actuators.

\begin{figure}[h]
 \centering
  \includegraphics[width=0.8\linewidth]{figures/SCADA.pdf}
  \caption{A diagram of a power-grid system. Attacks can happen in \ding{172} sensors \ding{173} actuators \ding{174} controllers and \ding{175} networks.}
  \label{fig:SCADA}
\end{figure}

\begin{figure}[h]
 \centering
  \includegraphics[width=\linewidth]{figures/PLCdiag.pdf}
  \caption{Programmable logic controller block diagram \cite{PLCdiag}}
  \label{fig:PLCdiag}
\end{figure}%

This problem of protecting the controllers in the power-grid system is a critical unsolved problem. It is challenging because \ding{172} there is no prior-knowledge about the attacks, especially in "zero-day" attacks. \ding{173} Various controllers, e.g. ABB, Siemens and Wago, and OS, e.g. Windows, Unix and proprietary OS (SIMATIC WinCC), are widely used in modern power-grid systems, exposing a large attack surface. \ding{174} Many concurrent threads are running on the controller simultaneously. Stealthy attack programs that have infected the controller can hide within these threads. \ding{175} Previous anomaly detection approaches transfer the data to a central server through networks, introducing a large attack surface and longer latency.%

Many works have proposed accelerating different aspects of machine learning and deep learning algorithms \cite{Chen:2014:DSH:2541940.2541967, chen2016diannao, esmaeilzadeh2012neural, wang2017dlau}. However, how much deep learning and acceleration is actually needed to achieve important security functions has not been studied. 

We propose an algorithm-architecture co-design methodology to apply deep learning for improving security in this security-critical system. To the best of our knowledge, this is the first paper to study what is the adequate amount of hardware acceleration needed for a real-world security problem versus accelerating one or more deep learning algorithms or components to the maximum possible. 

Our main contributions in this paper are:

\begin{itemize}
\item We propose a new data-driven, learning-based, statistically-enhanced algorithm for detecting anomalies in power-grid controllers. This algorithm requires only normal data for training the deep learning model. This is especially useful in security-critical situations, such as power-grid systems where the attack data are highly sensitive and hard to obtain.

\item We further propose Reconstruction Error Distribution (RED) that alleviate the need of understanding application-specific details and does not require very accurate prediction of normal behavior.

\item We find the minimum amount of hardware sufficient to solve important real world security problems rather than just going for highest performance of one algorithm or one component of a class of deep learning algorithms.

\item We show a clear methodology and essential steps in applying deep learning (or any AI technique) to solve security problems.
\end{itemize}

In Section \ref{sec:methodology}, we show our methodology in deep learning based algorithm-architecture co-design for security problems. In Section \ref{sec:SecurityConsiderations}, we articulate our threat model. In Section \ref{sec:solution}, we present our algorithm design in detail. In Section \ref{sec:implementation}, we show the hardware that is sufficient for implementing our deep learning algorithm. In Section \ref{sec:PerformanceEva} and \ref{sec:AccuracyEva}, we present the performance and accuracy evaluation, respectively. We discuss related work in Section \ref{sec:relatedwork} and conclude in Section \ref{sec:conclusion}.