\begin{thebibliography}{10}

\bibitem{PLCdiag}
Programmable logic controller block diagram.
\newblock \url{https://www.youtube.com/watch?v=oOCW5muXNyo}, 2010.

\bibitem{2015powergridattack}
December 2015 ukraine power grid cyberattack.
\newblock
  \url{https://en.wikipedia.org/wiki/December_2015_Ukraine_power_grid_cyberattack},
  2015.

\bibitem{SCADA}
The efficacy and challenges of scada and smart grid integration.
\newblock \url{https://www.csiac.org/journal-article/the-efficacy-and-\
  challenges-of-scada-and-smart-grid-integration/}, 2016.

\bibitem{asanovic2016rocket}
Krste Asanovic, Rimas Avizienis, Jonathan Bachrach, Scott Beamer, David
  Biancolin, Christopher Celio, Henry Cook, Daniel Dabbelt, John Hauser, Adam
  Izraelevitz, et~al.
\newblock The rocket chip generator.
\newblock {\em EECS Department, University of California, Berkeley, Tech. Rep.
  UCB/EECS-2016-17}, 2016.

\bibitem{bahador2014hpcmalhunter}
Mohammad~Bagher Bahador, Mahdi Abadi, and Asghar Tajoddin.
\newblock Hpcmalhunter: Behavioral malware detection using hardware performance
  counters and singular value decomposition.
\newblock In {\em Computer and Knowledge Engineering (ICCKE), 2014 4th
  International eConference on}, pages 703--708. IEEE, 2014.

\bibitem{breunig2000lof}
Markus~M Breunig, Hans-Peter Kriegel, Raymond~T Ng, and J{\"o}rg Sander.
\newblock Lof: identifying density-based local outliers.
\newblock In {\em ACM sigmod record}, volume~29, pages 93--104. ACM, 2000.

\bibitem{cardenas2011attacks}
Alvaro~A C{\'a}rdenas, Saurabh Amin, Zong-Syun Lin, Yu-Lun Huang, Chi-Yen
  Huang, and Shankar Sastry.
\newblock Attacks against process control systems: risk assessment, detection,
  and response.
\newblock In {\em Proceedings of the 6th ACM symposium on information, computer
  and communications security}, pages 355--366. ACM, 2011.

\bibitem{Chen:2014:DSH:2541940.2541967}
Tianshi Chen, Zidong Du, Ninghui Sun, Jia Wang, Chengyong Wu, Yunji Chen, and
  Olivier Temam.
\newblock Diannao: A small-footprint high-throughput accelerator for ubiquitous
  machine-learning.
\newblock In {\em Proceedings of the 19th International Conference on
  Architectural Support for Programming Languages and Operating Systems},
  ASPLOS '14, pages 269--284, New York, NY, USA, 2014. ACM.

\bibitem{chen2016diannao}
Yunji Chen, Tianshi Chen, Zhiwei Xu, Ninghui Sun, and Olivier Temam.
\newblock Diannao family: energy-efficient hardware accelerators for machine
  learning.
\newblock {\em Communications of the ACM}, 59(11):105--112, 2016.

\bibitem{demme2013feasibility}
John Demme, Matthew Maycock, Jared Schmitz, Adrian Tang, Adam Waksman, Simha
  Sethumadhavan, and Salvatore Stolfo.
\newblock On the feasibility of online malware detection with performance
  counters.
\newblock In {\em ACM SIGARCH Computer Architecture News}, volume~41, pages
  559--570. ACM, 2013.

\bibitem{dolgikh2012using}
Andrey Dolgikh, Tomas Nykodym, Victor Skormin, and Zachary Birnbaum.
\newblock Using behavioral modeling and customized normalcy profiles as
  protection against targeted cyber-attacks.
\newblock In {\em International Conference on Mathematical Methods, Models, and
  Architectures for Computer Network Security}, pages 191--202. Springer, 2012.

\bibitem{esmaeilzadeh2012neural}
Hadi Esmaeilzadeh, Adrian Sampson, Luis Ceze, and Doug Burger.
\newblock Neural acceleration for general-purpose approximate programs.
\newblock In {\em Proceedings of the 2012 45th Annual IEEE/ACM International
  Symposium on Microarchitecture}, pages 449--460. IEEE Computer Society, 2012.

\bibitem{fouda2011lightweight}
Mostafa~M Fouda, Zubair~Md Fadlullah, Nei Kato, Rongxing Lu, and Xuemin~Sherman
  Shen.
\newblock A lightweight message authentication scheme for smart grid
  communications.
\newblock {\em IEEE Transactions on Smart Grid}, 2(4):675--685, 2011.

\bibitem{gao2018deltarnn}
Chang Gao, Daniel Neil, Enea Ceolini, Shih-Chii Liu, and Tobi Delbruck.
\newblock Deltarnn: A power-efficient recurrent neural network accelerator.
\newblock In {\em Proceedings of the 2018 ACM/SIGDA International Symposium on
  Field-Programmable Gate Arrays}, pages 21--30. ACM, 2018.

\bibitem{goldstein2012histogram}
Markus Goldstein and Andreas Dengel.
\newblock Histogram-based outlier score (hbos): A fast unsupervised anomaly
  detection algorithm.
\newblock {\em KI-2012: Poster and Demo Track}, pages 59--63, 2012.

\bibitem{han2017ese}
Song Han, Junlong Kang, Huizi Mao, Yiming Hu, Xin Li, Yubin Li, Dongliang Xie,
  Hong Luo, Song Yao, Yu~Wang, et~al.
\newblock Ese: Efficient speech recognition engine with sparse lstm on fpga.
\newblock In {\em FPGA}, pages 75--84, 2017.

\bibitem{hochreiter1997long}
Sepp Hochreiter and J{\"u}rgen Schmidhuber.
\newblock Long short-term memory.
\newblock {\em Neural computation}, 9(8):1735--1780, 1997.

\bibitem{humayed2017cyber}
Abdulmalik Humayed, Jingqiang Lin, Fengjun Li, and Bo~Luo.
\newblock Cyber-physical systems security—a survey.
\newblock {\em IEEE Internet of Things Journal}, 4(6):1802--1831, 2017.

\bibitem{kriegel2008angle}
Hans-Peter Kriegel, Arthur Zimek, et~al.
\newblock Angle-based outlier detection in high-dimensional data.
\newblock In {\em Proceedings of the 14th ACM SIGKDD international conference
  on Knowledge discovery and data mining}, pages 444--452. ACM, 2008.

\bibitem{langner2011stuxnet}
Ralph Langner.
\newblock Stuxnet: Dissecting a cyberwarfare weapon.
\newblock {\em IEEE Security \& Privacy}, 9(3):49--51, 2011.

\bibitem{lee2015past}
Edward~A Lee.
\newblock The past, present and future of cyber-physical systems: A focus on
  models.
\newblock {\em Sensors}, 15(3):4837--4869, 2015.

\bibitem{liu2008isolation}
Fei~Tony Liu, Kai~Ming Ting, and Zhi-Hua Zhou.
\newblock Isolation forest.
\newblock In {\em Data Mining, 2008. ICDM'08. Eighth IEEE International
  Conference on}, pages 413--422. IEEE, 2008.

\bibitem{malhotra2016lstm}
Pankaj Malhotra, Anusha Ramakrishnan, Gaurangi Anand, Lovekesh Vig, Puneet
  Agarwal, and Gautam Shroff.
\newblock Lstm-based encoder-decoder for multi-sensor anomaly detection.
\newblock {\em arXiv preprint arXiv:1607.00148}, 2016.

\bibitem{malhotra2015long}
Pankaj Malhotra, Lovekesh Vig, Gautam Shroff, and Puneet Agarwal.
\newblock Long short term memory networks for anomaly detection in time series.
\newblock In {\em Proceedings}, page~89. Presses universitaires de Louvain,
  2015.

\bibitem{manandhar2014detection}
Kebina Manandhar, Xiaojun Cao, Fei Hu, and Yao Liu.
\newblock Detection of faults and attacks including false data injection attack
  in smart grid using kalman filter.
\newblock {\em IEEE transactions on control of network systems}, 1(4):370--379,
  2014.

\bibitem{ozsoy2016hardware}
Meltem Ozsoy, Khaled~N Khasawneh, Caleb Donovick, Iakov Gorelik, Nael
  Abu-Ghazaleh, and Dmitry Ponomarev.
\newblock Hardware-based malware detection using low-level architectural
  features.
\newblock {\em IEEE Transactions on Computers}, 65(11):3332--3344, 2016.

\bibitem{patel2017analyzing}
Nisarg Patel, Avesta Sasan, and Houman Homayoun.
\newblock Analyzing hardware based malware detectors.
\newblock In {\em Proceedings of the 54th Annual Design Automation Conference
  2017}, page~25. ACM, 2017.

\bibitem{rousseeuw1999fast}
Peter~J Rousseeuw and Katrien~Van Driessen.
\newblock A fast algorithm for the minimum covariance determinant estimator.
\newblock {\em Technometrics}, 41(3):212--223, 1999.

\bibitem{scholkopf2001estimating}
Bernhard Sch{\"o}lkopf, John~C Platt, John Shawe-Taylor, Alex~J Smola, and
  Robert~C Williamson.
\newblock Estimating the support of a high-dimensional distribution.
\newblock {\em Neural computation}, 13(7):1443--1471, 2001.

\bibitem{sikdar2011defending}
Biplab Sikdar and Joe~H Chow.
\newblock Defending synchrophasor data networks against traffic analysis
  attacks.
\newblock {\em IEEE Transactions on Smart Grid}, 2(4):819--826, 2011.

\bibitem{simpson1997introduction}
DG~Simpson.
\newblock Introduction to rousseeuw (1984) least median of squares regression.
\newblock In {\em Breakthroughs in Statistics}, pages 433--461. Springer, 1997.

\bibitem{singh2017detection}
Baljit Singh, Dmitry Evtyushkin, Jesse Elwell, Ryan Riley, and Iliano
  Cervesato.
\newblock On the detection of kernel-level rootkits using hardware performance
  counters.
\newblock In {\em Proceedings of the 2017 ACM on Asia Conference on Computer
  and Communications Security}, pages 483--493. ACM, 2017.

\bibitem{song2018situ}
Mingcong Song, Kan Zhong, Jiaqi Zhang, Yang Hu, Duo Liu, Weigong Zhang, Jing
  Wang, and Tao Li.
\newblock In-situ ai: Towards autonomous and incremental deep learning for iot
  systems.
\newblock In {\em 2018 IEEE International Symposium On High Performance
  Computer Architecture (HPCA)}, pages 92--103. IEEE, 2018.

\bibitem{sridhar2012cyber}
Siddharth Sridhar, Adam Hahn, and Manimaran Govindarasu.
\newblock Cyber--physical system security for the electric power grid.
\newblock {\em Proceedings of the IEEE}, 100(1):210--224, 2012.

\bibitem{valenzuela2013real}
Jorge Valenzuela, Jianhui Wang, and Nancy Bissinger.
\newblock Real-time intrusion detection in power system operations.
\newblock {\em IEEE Transactions on Power Systems}, 28(2):1052--1062, 2013.

\bibitem{wang2017dlau}
Chao Wang, Lei Gong, Qi~Yu, Xi~Li, Yuan Xie, and Xuehai Zhou.
\newblock Dlau: A scalable deep learning accelerator unit on fpga.
\newblock {\em IEEE Transactions on Computer-Aided Design of Integrated
  Circuits and Systems}, 36(3):513--517, 2017.

\bibitem{wang2018c}
Shuo Wang, Zhe Li, Caiwen Ding, Bo~Yuan, Qinru Qiu, Yanzhi Wang, and Yun Liang.
\newblock C-lstm: Enabling efficient lstm using structured compression
  techniques on fpgas.
\newblock In {\em Proceedings of the 2018 ACM/SIGDA International Symposium on
  Field-Programmable Gate Arrays}, pages 11--20. ACM, 2018.

\bibitem{wang2011security}
Xudong Wang and Ping Yi.
\newblock Security framework for wireless communications in smart distribution
  grid.
\newblock {\em IEEE Transactions on Smart Grid}, 2(4):809--818, 2011.

\bibitem{wang2016hardware}
Xueyang Wang, Sek Chai, Michael Isnardi, Sehoon Lim, and Ramesh Karri.
\newblock Hardware performance counter-based malware identification and
  detection with adaptive compressive sensing.
\newblock {\em ACM Transactions on Architecture and Code Optimization (TACO)},
  13(1):3, 2016.

\bibitem{wang2013numchecker}
Xueyang Wang and Ramesh Karri.
\newblock Numchecker: Detecting kernel control-flow modifying rootkits by using
  hardware performance counters.
\newblock In {\em Proceedings of the 50th Annual Design Automation Conference},
  page~79. ACM, 2013.

\bibitem{wang2015confirm}
Xueyang Wang, Charalambos Konstantinou, Michail Maniatakos, and Ramesh Karri.
\newblock Confirm: Detecting firmware modifications in embedded systems using
  hardware performance counters.
\newblock In {\em Proceedings of the IEEE/ACM International Conference on
  Computer-Aided Design}, pages 544--551. IEEE Press, 2015.

\bibitem{wei2005assumption}
Li~Wei, Nitin Kumar, Venkata~Nishanth Lolla, Eamonn~J Keogh, Stefano Lonardi,
  and Chotirat~(Ann) Ratanamahatana.
\newblock Assumption-free anomaly detection in time series.
\newblock In {\em SSDBM}, volume~5, pages 237--242, 2005.

\bibitem{zhao2014real}
Feng Zhao, Guannan Wang, Chunyu Deng, and Yue Zhao.
\newblock A real-time intelligent abnormity diagnosis platform in electric
  power system.
\newblock In {\em Advanced Communication Technology (ICACT), 2014 16th
  International Conference on}, pages 83--87. IEEE, 2014.

\end{thebibliography}
