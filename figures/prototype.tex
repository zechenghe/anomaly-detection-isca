

\newcommand{\bheading}[1]{{\vspace{2pt}\noindent{\textbf{#1}}\hspace{2pt}}}

\section{Implementation}
\subsection{FPGA Prototype}

We design a hardware module to enable the always-on anomaly detection. The module incurs minimal resource overhead by implementing only the essential computation primitives needed by LSTM inferencing and KS-test. The module is designed to be microcode-driven so that it is also programmable to support various models. We prove in Section \ref{sec:PerformanceEva} that the module has enough performance for our detection mechanism.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{./figures/detection_module.pdf}
    \caption{Block diagram of the detection module}
    \label{img_blockdiagram}
\end{figure}

The architecture of our anomaly detection module is plotted in Figure~\ref{img_blockdiagram}. The processing part of the module is pipelined to have four execution stages. Look-up tables (LUTs) of non-linear functions are placed in the first execution stage. The second and third stage include multipliers (MULs) and adders (ADDs) respectively. The final execution stage is only used to latch the outputs to the external block RAM. The block RAM is a piece of addressable memory used to stored model parameters and intermediate variables during computation. The memory space of the block RAM and the address generation are managed by the microcodes of a ML/DL model. There is also a buffer to store the incoming HPC readings. The buffer is connected to the hardware module and mapped to a different region in the microcode's address space. This enables the hardware module to be programmed to fetch the input features.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{./figures/lstm_eq_fig.pdf}
    \caption{Operations for LSTM inference}
    \label{img_lstm_eq_fig}
\end{figure}

\subsubsection{Support for LSTM Inference} ~\\
The microcode is designed to instruct the datapath to perform all the necessary operation primitives. We demonstrate the operations for LSTM inference in Figure~\ref{img_lstm_eq_fig}. To support these computation primitives, we incorporate the following modes in which the detection module can operate: 

\bheading{Element-wise addition and subtraction of two vectors.} The input vectors will be forwarded to the EXE2 stage for addition (\textbf{Vadd}). The subtraction (\textbf{Vsub}) is computed by feeding the opposite of the second operand to the adder.

\bheading{Element-wise multiplication of two vectors.} In vector multiplication mode (\textbf{Vmul}), the input vectors will be forwarded to the multiplier stage (EXE1).

\bheading{Element-wise non linear function of a vector.} Activation functions like sigmoid and tanh are used in the LSTM model (Vact). Instead of adding complicated function units, we approximate the activation functions with linear interpolation. When an input vector comes in, the slope (slp[i]) and the intercept (b[i]) of a piecewise linear approximation are selected from every LUT in EXE0 execution stage according to the input value (X[i]). The result (Z[i]) is computed in the following two stages (EXE1 and EXE2) as Equation~(\ref{eq_vact}). Different from the work~\cite{Chen:2014:DSH:2541940.2541967} which places the non-linear units in the last stage and thus needs extra multipliers and adders, this design reduces the design cost by placing the LUTs in an earlier stage so that existing multipliers and adders in later stages are reused.

\begin{equation}\label{eq_vact}
Z[i] = slp[i] \times X[i] + b[i]
\end{equation}

\bheading{Matrix-vector multiplication.} In the LSTM model, feature vectors need to be multiplied with weight matrices. To perform this operation, the detection module first computes the element-wise products of a row in the matrix and the vector in EXE1 stage. The products are then summed up and added to the partial sum of the current row. We implement two optimizations in this module to further reduce the design cost and improve performance:

\textbf{\ding{172} Adder reuse.} Without adding more adders to sum up the products, we make the adders in EXE2 stage controllable to act as an adder tree which takes the products and the previous partial sum as its input.

\textbf{\ding{173} Loop tiling and local storage.} A loop tiling for matrix-vector multiplication is described in Listing~\ref{alg_tiling}. It is a technique to reduce memory traffic by unrolling a loop and increasing data reuse. To better utilize the reuse of output data, the module saves repetitive read-and-refetch of partial sums (\textbf{O[i*N\_local+ii]}) by storing them in the local scratchpad. During the computation, the partial sum are read from the local scratchpad and used as one of the inputs to the adder tree.

\begin{figure}[h]
\begin{lstlisting}[language=C, frame=single, xleftmargin=.5em, framexleftmargin=.5em, caption={Tiling for Matrix-vector Multiplication}, label={alg_tiling}]
  // N_local: size of local scratchpad
  // N_track: number of parallel computing tracks
  for (i = 0; i < L; i+= N_local)
    for (j = 0; j < W; j += N_track)
      for (ii = 0; ii < N_local; ii ++)
        // Computed every cycle
        for (jj = 0; jj < N_track; jj++)
          O[i*N_local+ii] += W[i*N_local+ii][j*N_track+jj] * I[j*N_track+jj]
\end{lstlisting}
\end{figure}

\begin{figure}[h]
\begin{lstlisting}[language=C, frame=single, xleftmargin=.5em, framexleftmargin=.5em, caption={KS-test Implementation}, label={alg_kstest}]
  // Step 1: Get test histogram
  for (i = 0; i < L_test; i++)
    for (j = 0; j < L_ref; j ++)
      // Need vector-scalar comparison
      if test_err[i] < ref_bin[j]
        test_hist[j]++
  
  // Step 2: Compare with reference histogram
  for (i = 0; i < L_ref; i++)
    diff_hist[i] = ref_hist[i] - test_hist[i]
  
  // Step 3: Find maximum difference
  // Need to find the maximum in a vector
  diff_max = max(diff_hist)
  
  // Step 4: Report result
  if (diff_max > diff_th)
    report_anomaly(diff_max)
  else
    report_normal(diff_max)
\end{lstlisting}
\end{figure}

\bheading{Storage requirements.} The trained deep learning model and the reference RED need to be stored. In our FPGA implementation, the deep learning model is an LSTM with 128 hidden nodes, 325.1KB in size. The reference RED is less than 1KB in size. In total we need $<$ 326 KB storage, which fits in our FPGA memory.

\subsubsection{Support for Statistic Test} ~\\
We also consider the operations need by the KS-test implementation as Listing~\ref{alg_kstest} where a reference distribution of reconstruction errors and the test distribution will be compared. The reference distribution is passed to the hardware module as a set of parameters. The distribution parameters include a list of discrete bin boundaries (\textbf{ref\_bin}) and a reference cumulative histogram (\textbf{ref\_hist}). We perform a KS-test for a series of computed errors (\textbf{test\_err}) by (1) adding to the test histogram (\textbf{test\_hist}) for every input error (2) computing the difference between the test histogram and the reference histogram for all the bins and (3) finding the maximum difference. To support this three-step statistical test, we need two more operation primitives:

\bheading{Vector-scalar comparison.} The test error needs to be compared with every bin boundary of the reference histogram and the result will be accumulated in the test histogram. The vector scalar comparison is performed as an element-wise set-larger-than. The scalar is kept in the pipeline and the bin boundaries are streamed in for comparison. Each adder computes a difference by doing subtraction and the output is set to one if the difference is positive or zero if negative. The output could thus be understood as the histogram for one error.

\bheading{Find the maximum of a vector.} To find the maximum of a vector, the detection module compares the input elements in EXE2 stage by doing subtraction with the adders. A temporary maximum needs to be stored in the local scratch-pad as the traversal on the vector may take multiple cycles. In each cycle, the temporary maximum is also compared with the elements being processed and gets updated until there is no other input element.